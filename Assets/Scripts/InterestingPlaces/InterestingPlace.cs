﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class InterestingPlace : MonoBehaviour
{
    BoxCollider boxCollider;

    protected virtual void Start() {
        boxCollider = gameObject.AddComponent<BoxCollider>();
        boxCollider.size = Vector3.one;
        boxCollider.isTrigger = true;
    }

    protected virtual void OnTriggerEnter(Collider colilder)
    {
        if (colilder.CompareTag("Player") || colilder.CompareTag("AI")) {
            (int, int) position = (Mathf.RoundToInt(transform.position.z), Mathf.RoundToInt(transform.position.x));
            colilder.GetComponentInParent<AgentMemory>().AddInformation(this, position);
        }
    }

    protected void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponentInParent<AgentBrain>().FillNeed(this);
        }
    }

    public abstract Slider GetSlider();

}