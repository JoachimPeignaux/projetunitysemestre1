﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodPlace : InterestingPlace
{
    static public Slider slider;

    public override Slider GetSlider()
    {
        return slider;
    }

    protected override void Start()
    {
        slider = GameObject.FindGameObjectWithTag("Hunger").GetComponent<Slider>();
        base.Start();
    }
}
