﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HUDManager : MonoBehaviour
{
    public enum GameState { thinking, running};
    [SerializeField] Text stateText;
    Queue<string> historyTexts = new Queue<string>(0);
    [SerializeField] Text percentage;
    [SerializeField] Text oldHistory;
    [SerializeField] Text newHistory;
    static public HUDManager instance = null;
    float _historyVisibleDuration = 5f;

    private void Awake() {
        instance = this;
    }

    public void ChangeState(GameState state) {
        switch (state) {
            case GameState.thinking:
                stateText.text = "BlueAgent is thinking about a path...";
                break;
            case GameState.running:
                stateText.text = "BlueAgent is running to the destination!";
                break;
        }
    }

    public void SetPercentage(float percentageValue) {
        percentage.text = percentageValue.ToString("0.00") + "%";
    }

    public void SetHistory(string text) {
        StopCoroutine(HideHistory());
        oldHistory.transform.parent.gameObject.SetActive(true);

        string newText = "";
        for(int i=0; i<historyTexts.Count - 1; i++) {
            newText += historyTexts.ToArray()[i] + '\n';
        }

        if (historyTexts.Count == 4)
        {
            historyTexts.Dequeue();
        }
        historyTexts.Enqueue(text);

        oldHistory.text = newText;
        newHistory.text = text;
        StartCoroutine(HideHistory());
    }

    IEnumerator HideHistory() {
        yield return new WaitForSeconds(_historyVisibleDuration);
        oldHistory.transform.parent.gameObject.SetActive(false);
    }
}
