﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureGenerator
{
    public Texture2D GetMazeTexture(bool[,] mazeGride) {
        Texture2D texture = new Texture2D(mazeGride.GetLength(1), mazeGride.GetLength(0));
        texture.filterMode = FilterMode.Point;

        for (int j = 0; j < mazeGride.GetLength(0); j++) {
            for (int i = 0; i < mazeGride.GetLength(1); i++) {
                if (mazeGride[j, i])
                    texture.SetPixel(i, j, Color.white);
                else
                    texture.SetPixel(i, j, Color.black);
            }
        }
        texture.Apply();
        return texture;
    }
}
