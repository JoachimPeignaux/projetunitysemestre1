﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MeshGenerator {

    const float height = 1;

    public Mesh GetFullMazeMesh(bool[,] grid) {

        Mesh mazeMesh = new Mesh();
        mazeMesh.Clear();
        List<CombineInstance> listOfMeshes = new List<CombineInstance>();

        int height = grid.GetLength(0);
        int width = grid.GetLength(1);

        bool[,] gridCopy = new bool[height, width];
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                gridCopy[j, i] = grid[j, i];
            }
        }

        //ballayage horizontal
        for (int j = 0; j < height; j++)
        {
            for (int i = 0; i < width; i++)
            {
                if (gridCopy[j, i] == false) {
                    if (i+1 < width  && gridCopy[j, i + 1] == false) {
                        gridCopy[j, i] = true;
                        gridCopy[j, i + 1] = true;
                        (int, int) from = (j, i);
                        (int, int) to = (j, i + 1);
                        int size = 2;
                        while ((i+size) < width && gridCopy[j, i+size] == false) {
                            to.Item2++;
                            gridCopy[j, i + size] = true;
                            size++;
                        }
                        CombineInstance combineInstance = new CombineInstance();
                        if (to.Item2 + 1 < width)
                            gridCopy[to.Item1, to.Item2 + 1] = true;
                        combineInstance.mesh = GetSubMesh(from, (to.Item1, to.Item2 + 1));
                        combineInstance.transform = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
                        listOfMeshes.Add(combineInstance);
                        i += size+1;//
                    }
                }
            }
        }

        //vertical
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if (gridCopy[j, i] == false)
                {
                    if (j + 1 < height)
                    {
                        gridCopy[j, i] = true;
                        (int, int) from = (j, i);
                        (int, int) to = (j, i);
                        int size = 1;
                        while ((j + size) < height && gridCopy[j + size, i] == false)
                        {
                            gridCopy[j+size, i] = true;
                            to.Item1++;
                            size++;
                        }
                        CombineInstance combineInstance = new CombineInstance();
                        if(to.Item1+1 < height)
                            gridCopy[to.Item1+1, to.Item2] = true;
                        combineInstance.mesh = GetSubMesh(from, (to.Item1+1, to.Item2));
                        combineInstance.transform = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, Vector3.one);
                        listOfMeshes.Add(combineInstance);
                        j += size;
                    }
                }
            }
        }

        mazeMesh.CombineMeshes(listOfMeshes.ToArray());
        mazeMesh.RecalculateNormals();
        return mazeMesh;
    }

    public Mesh GetSubMesh((int, int) from, (int, int) to) {

        Mesh mesh = new Mesh();

        Vector3[] vertices = GetStringVertices(from, to);
        int verticesCount = vertices.GetLength(0);
        int[] triangles = new int[(verticesCount/2)*3];

        int indexOfVertices = 0;
        int indexOfTriangle = 0;

        while (indexOfVertices < verticesCount - 1) {
            triangles[indexOfTriangle     ] = indexOfVertices;
            triangles[indexOfTriangle + 1 ] = indexOfVertices + 1;
            triangles[indexOfTriangle + 2 ] = indexOfVertices + 2;
                                        
            triangles[indexOfTriangle + 3 ] = indexOfVertices;
            triangles[indexOfTriangle + 4 ] = indexOfVertices + 2;
            triangles[indexOfTriangle + 5 ] = indexOfVertices + 3;

            indexOfTriangle += 6;
            indexOfVertices += 4;
        }

        mesh.vertices = vertices;
        mesh.triangles = triangles;
        return mesh;
    }

    Vector3[] GetStringVertices((int, int) from, (int, int) to) {
        Vector3[] vertices = new Vector3[24];

        int indexOfVertices = 0;

        foreach (Vector3 vertex in GetFrontFace(from, to)) {
            vertices[indexOfVertices] = vertex;
            indexOfVertices++;
        }

        foreach (Vector3 vertex in GetBackFace(from, to))
        {
            vertices[indexOfVertices] = vertex;
            indexOfVertices++;
        }

        foreach (Vector3 vertex in GetLeftFace(from, to))
        {
            vertices[indexOfVertices] = vertex;
            indexOfVertices++;
        }

        foreach (Vector3 vertex in GetRightFace(from, to))
        {
            vertices[indexOfVertices] = vertex;
            indexOfVertices++;
        }

        foreach (Vector3 vertex in GetTopFace(from, to))
        {
            vertices[indexOfVertices] = vertex;
            indexOfVertices++;
        }

        foreach (Vector3 vertex in GetBottomFace(from, to))
        {
            vertices[indexOfVertices] = vertex;
            indexOfVertices++;
        }

        return vertices;

    }

    List<Vector3> GetTopFace((int, int) from, (int, int) to) { //j,i
        List<Vector3> vertices;
        if (from.Item1 == to.Item1) {  //horizontal
            vertices = new List<Vector3> {
                new Vector3 (from.Item1, height, from.Item2), //1
                new Vector3 (from.Item1, height, to.Item2), //4
                new Vector3 (from.Item1 + 1, height, to.Item2), //3
                new Vector3 (from.Item1 + 1, height, from.Item2) //2
            };
            return vertices;
        } else if (from.Item2 == to.Item2) { //vertical
            vertices = new List<Vector3> {
                new Vector3(from.Item1, height, from.Item2),
                new Vector3(from.Item1, height, from.Item2 + 1),
                new Vector3(to.Item1, height, from.Item2 + 1),
                new Vector3(to.Item1, height, from.Item2)

            };
            return vertices;
        }
        throw new Exception("Bad Vertice Coordinate for the maze mesh, from : " + from + ", to : " + to);
    }
    
    List<Vector3> GetBottomFace((int, int) from, (int, int) to)
    { //j,i
        List<Vector3> vertices;
        if (from.Item1 == to.Item1)  //horizontal
        {
            vertices = new List<Vector3> {
                new Vector3 (from.Item1, 0, from.Item2),
                new Vector3 (from.Item1 + 1, 0, from.Item2),
                new Vector3 (from.Item1 + 1, 0, to.Item2),
                new Vector3 (from.Item1, 0, to.Item2)
            };
            return vertices;
        }
        else if (from.Item2 == to.Item2)
        {
            vertices = new List<Vector3> {
                new Vector3(from.Item1, 0, from.Item2),
                new Vector3(to.Item1, 0, from.Item2),
                new Vector3(to.Item1, 0, from.Item2 + 1),
                new Vector3(from.Item1, 0, from.Item2 + 1)
            };
            return vertices;
        }
        throw new Exception("Bad Vertice Coordinate for the maze mesh, from : " + from + ", to : " + to);
    }
    
    List<Vector3> GetFrontFace((int, int) from, (int, int) to)
    { //j,i
        List<Vector3> vertices;
        if (from.Item1 == to.Item1)  //horizontal
        {
            vertices = new List<Vector3> {
                new Vector3 (from.Item1 + 1, height, from.Item2),
                new Vector3 (from.Item1 + 1, height, to.Item2),
                new Vector3 (from.Item1 + 1, 0, to.Item2),
                new Vector3 (from.Item1 + 1, 0, from.Item2)
            };
            return vertices;
        }
        else if (from.Item2 == to.Item2)
        {
            vertices = new List<Vector3> {
                new Vector3(to.Item1, height, to.Item2),
                new Vector3(to.Item1, height, to.Item2 + 1 ),
                new Vector3(to.Item1, 0, to.Item2 + 1),
                new Vector3(to.Item1, 0, to.Item2)
            };
            return vertices;
        }
        throw new Exception("Bad Vertice Coordinate for the maze mesh, from : " + from + ", to : " + to);
    }
    
    List<Vector3> GetBackFace((int, int) from, (int, int) to)
    { //j,i
        List<Vector3> vertices;
        if (from.Item1 == to.Item1)  //horizontal
        {
            vertices = new List<Vector3> {
                new Vector3 (from.Item1, height, from.Item2),
                new Vector3 (from.Item1, 0, from.Item2),
                new Vector3 (from.Item1, 0, to.Item2),
                new Vector3 (from.Item1, height, to.Item2)
            };
            return vertices;
        }
        else if (from.Item2 == to.Item2)
        {
            vertices = new List<Vector3> {
                new Vector3(from.Item1, height, from.Item2),
                new Vector3(from.Item1, 0, from.Item2),
                new Vector3(from.Item1, 0, from.Item2 + 1),
                new Vector3(from.Item1, height, from.Item2 + 1)
            };
            return vertices;
        }
        throw new Exception("Bad Vertice Coordinate for the maze mesh, from : " + from + ", to : " + to);
    }
    
    List<Vector3> GetLeftFace((int, int) from, (int, int) to)
    { //j,i
        List<Vector3> vertices;
        if (from.Item1 == to.Item1)
        {  //horizontal
            vertices = new List<Vector3> {
                new Vector3 (from.Item1+1, height, from.Item2),
                new Vector3 (from.Item1+1, 0, from.Item2),
                new Vector3 (from.Item1 , 0, from.Item2),
                new Vector3 (from.Item1, height, from.Item2)
            };
            return vertices;
        }
        else if (from.Item2 == to.Item2)
        { //vertical
            vertices = new List<Vector3> {
                new Vector3 (from.Item1, height, to.Item2+1),
                new Vector3 (from.Item1, 0, to.Item2+1),
                new Vector3 (to.Item1 , 0, to.Item2+1),
                new Vector3 (to.Item1, height, to.Item2+1)
            };
            return vertices;
        }
        throw new Exception("Bad Vertice Coordinate for the maze mesh, from : " + from + ", to : " + to);
    }
    
    List<Vector3> GetRightFace((int, int) from, (int, int) to)
    { //j,i
        List<Vector3> vertices;
        if (from.Item1 == to.Item1)
        {  //horizontal
            vertices = new List<Vector3> {
                new Vector3 (to.Item1+1, height, to.Item2),
                new Vector3 (to.Item1, height, to.Item2),
                new Vector3 (to.Item1 , 0, to.Item2),
                new Vector3 (to.Item1+1, 0, to.Item2),

            };
            return vertices;
        }
        else if (from.Item2 == to.Item2)
        { //vertical
            vertices = new List<Vector3> {
                new Vector3 (from.Item1, height, to.Item2),
                new Vector3 (to.Item1, height, to.Item2),
                new Vector3 (to.Item1 , 0, to.Item2),
                new Vector3 (from.Item1, 0, to.Item2)
            };
            return vertices;
        }
        throw new Exception("Bad Vertice Coordinate for the maze mesh, from : " + from + ", to : " + to);
    }

}