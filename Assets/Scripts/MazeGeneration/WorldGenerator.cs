﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class WorldGenerator : MonoBehaviour
{
    private GameObject _mazeObject;

    [SerializeField] private Material MAZE_MATERIAL;
    [SerializeField] static public int SIZE = 101;
    [SerializeField] private InterestingPlaceData[] _interestingPlaces;

    private enum RenderMode { _3D, _2D };
    [SerializeField] private RenderMode RENDER_MODE;
    private MazeGenerator _mazeGenerator;

    void Awake()
    {
        if (SIZE % 2 == 0)
        {
            SIZE += 1;
        }
        _mazeGenerator = new MazeGenerator(SIZE);
        bool[,] grideMaze = _mazeGenerator.GetGride();
        PathFounder pathFounderInit = new PathFounder(grideMaze);

        if (RENDER_MODE == RenderMode._3D)
        {
            Create3DMaze(grideMaze);
        }
        else if (RENDER_MODE == RenderMode._2D)
        {
            Create2DMaze(grideMaze);
        }
        GenerateInterestingPlaces();
    }

    private void Create3DMaze(bool[,] grideMaze)
    {
        GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        plane.GetComponent<MeshRenderer>().material = MAZE_MATERIAL;
        float mazeSize = plane.GetComponent<MeshFilter>().mesh.bounds.size.x;
        float mazeSizeRatio = SIZE / mazeSize;
        plane.transform.localScale = new Vector3(mazeSizeRatio, 1, mazeSizeRatio);
        plane.transform.Translate(SIZE / 2, 0, SIZE / 2);
        plane.transform.Rotate(Vector3.up, 180);
        plane.GetComponent<MeshRenderer>().material.mainTexture = new TextureGenerator().GetMazeTexture(grideMaze);

        _mazeObject = new GameObject("Maze", typeof(MeshFilter), typeof(MeshRenderer));
        _mazeObject.GetComponent<MeshRenderer>().material = MAZE_MATERIAL;
        MeshGenerator meshGenerator = new MeshGenerator();
        _mazeObject.GetComponent<MeshFilter>().mesh = meshGenerator.GetFullMazeMesh(grideMaze);
        _mazeObject.transform.Rotate(Vector3.up, 90);
        _mazeObject.transform.localScale = new Vector3(-1, 1, 1);
        _mazeObject.transform.position = new Vector3(-0.5f, 0, -0.5f);
        _mazeObject.layer = 8;
    }

    private void Create2DMaze(bool[,] grideMaze) {
        _mazeObject = GameObject.CreatePrimitive(PrimitiveType.Plane);
        _mazeObject.GetComponent<MeshRenderer>().material = MAZE_MATERIAL;
        _mazeObject.GetComponent<MeshRenderer>().material.mainTexture = new TextureGenerator().GetMazeTexture(grideMaze);
        float mazeSize = _mazeObject.GetComponent<MeshFilter>().mesh.bounds.size.x;
        float mazeSizeRatio = SIZE / mazeSize;
        _mazeObject.transform.localScale = new Vector3(mazeSizeRatio, 1, mazeSizeRatio);
        _mazeObject.transform.Translate(SIZE / 2, 0, SIZE / 2);
        _mazeObject.transform.Rotate(Vector3.up, 180);
    }

    private void GenerateInterestingPlaces() {
        foreach (InterestingPlaceData placeData in _interestingPlaces) {
            for (int ind = 0; ind < placeData._instanceNbr; ind++) {
                int j = (int)Random.Range(0, (SIZE/2)-1);
                int i = (int)Random.Range(0, (SIZE / 2) - 1);
                GameObject.Instantiate(placeData._prefab, new Vector3(i*2+1,0,j*2+1), Quaternion.identity);
            }
        }
    }

}

[System.Serializable]
public class InterestingPlaceData {
    public GameObject _prefab;
    public int _instanceNbr;
}
