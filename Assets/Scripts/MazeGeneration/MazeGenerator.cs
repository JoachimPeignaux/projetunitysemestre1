﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class MazeGenerator
{
    private readonly int SIZE;
    private readonly int[,] _Gride;
    private readonly List<Tuple<int, int>> _breakableWalls = new List<Tuple<int, int>>();
    private int _numberOfWall = 0;

    public MazeGenerator(int size)
    {
        SIZE = size;
        _Gride = new int[SIZE, SIZE];
        InitMaze();
        BuildLabyrinth();
    }

    //false for walls, true for free
    public bool[,] GetGride()
    {
        bool[,] boolGride = new bool[SIZE, SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                boolGride[j, i] = (_Gride[j, i] == (int)CaseState.free);
            }
        }
        return boolGride;
    }

    private void InitMaze()
    {
        int caseValue = (int)CaseState.free;
        for (int j = 0; j < SIZE; j++)
        {
            for (int i = 0; i < SIZE; i++)
            {
                if (i % 2 == 0 && j % 2 == 0)
                {
                    _Gride[j, i] = (int)CaseState.permWall;
                    _numberOfWall++;
                }
                else if (i % 2 == 0 || j % 2 == 0)
                {
                    _Gride[j, i] = (int)CaseState.breakableWall;
                    _numberOfWall++;
                    if (i != 0 && i != SIZE-1 && j != 0 && j!= SIZE-1)
                        _breakableWalls.Add(new Tuple<int, int>(j, i));
                }
                else
                {
                    _Gride[j, i] = caseValue;
                    caseValue++;
                }
            }
        }
    }

    bool CanDeleteWall(int j, int i)
    {
        TestCoord(j, i);
        if (j % 2 == 1 && i % 2 == 0)
        {
            return (_Gride[j, i - 1] != _Gride[j, i + 1]);
        }
        else if (i % 2 == 1 && j % 2 == 0)
        {
            return (_Gride[j - 1, i] != _Gride[j + 1, i]);
        }
        else
        {
            return true;
        }
    }

    List<Tuple<int, int>> GetCaseByNumber(int number)
    {
        List<Tuple<int, int>> list_of_case = new List<Tuple<int, int>>();
        for (int j = 1; j < SIZE; j++)
        {
            for (int i = 1; i < SIZE; i++)
            {
                if (_Gride[j, i] == number)
                {
                    list_of_case.Add(new Tuple<int, int>(j, i));
                }
            }
        }
        return list_of_case;
    }

    void BreakWall(int j, int i)
    {
        int min = 0;
        if (j % 2 == 1)
        {
            if (_Gride[j, i - 1] < _Gride[j, i + 1])
            {
                min = _Gride[j, i - 1];
                List<Tuple<int, int>> list_of_cases = GetCaseByNumber(_Gride[j, i + 1]);
                for (int ind = 0; ind < list_of_cases.Count; ind++)
                {
                    _Gride[list_of_cases[ind].Item1, list_of_cases[ind].Item2] = min;
                }
            }
            else
            {
                min = _Gride[j, i + 1];
                List<Tuple<int, int>> list_of_cases = GetCaseByNumber(_Gride[j, i - 1]);
                for (int ind = 0; ind < list_of_cases.Count; ind++)
                {
                    _Gride[list_of_cases[ind].Item1, list_of_cases[ind].Item2] = min;
                }
            }
        }
        else if (i % 2 == 1)
        {
            if (_Gride[j - 1, i] < _Gride[j + 1, i])
            {
                min = _Gride[j - 1, i];
                List<Tuple<int, int>> list_of_cases = GetCaseByNumber(_Gride[j + 1, i]);
                for (int ind = 0; ind < list_of_cases.Count; ind++)
                {
                    _Gride[list_of_cases[ind].Item1, list_of_cases[ind].Item2] = min;
                }
            }
            else
            {
                min = _Gride[j + 1, i];
                List<Tuple<int, int>> list_of_cases = GetCaseByNumber(_Gride[j - 1, i]);
                for (int ind = 0; ind < list_of_cases.Count; ind++)
                {
                    _Gride[list_of_cases[ind].Item1, list_of_cases[ind].Item2] = min;
                }
            }
        }
        _Gride[j, i] = min;
        _numberOfWall--;
    }

    private void BuildLabyrinth()
    {
        int number_of_wall_to_break = (SIZE / 2) * (SIZE / 2) - 1;
        int number_of_broken_wall = 0;
        System.Random random = new System.Random((int)DateTime.Now.Millisecond);
        while (number_of_broken_wall < number_of_wall_to_break)
        {
            int pos = random.Next(_breakableWalls.Count);
            if (CanDeleteWall(_breakableWalls[pos].Item1, _breakableWalls[pos].Item2))
            {
                BreakWall(_breakableWalls[pos].Item1, _breakableWalls[pos].Item2);
                number_of_broken_wall++;
            }
            _breakableWalls.RemoveAt(pos);
        }

    }

    private void TestCoord(int j, int i)
    {
        if (i < 0 || j < 0 || i >= SIZE || j >= SIZE)
            throw new Exception("Baad coordonnées were given for the maze generation : "+j +"," +i);
    }

    public int GetNumberOfWall() {
        return _numberOfWall;
    }

}

public enum CaseState : int
{
    permWall = -1,
    breakableWall = 0,
    free = 1
};
