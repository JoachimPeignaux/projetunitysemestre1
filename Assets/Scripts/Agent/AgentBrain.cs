﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AgentBrain : MonoBehaviour
{
    public Need[] _needs;
    private float normalizedBarActivationNeedValue = 0.66f;

    private void Update() {
        foreach (Need need in _needs) {
            need.UpdateNeed();
        }
    }

    public void FillNeed( InterestingPlace place) {
        Type typeOfNeed = place.GetType();
        foreach (Need need in _needs)
        {
            if (need.GetTypeOfNeed().GetType() == typeOfNeed) {
                need.Fill();
                break;
            }
        }
    }
    public InterestingPlace GetMostImportantNeed()
    {
        float minValue = float.MaxValue;
        InterestingPlace interestingPlace = null;
        foreach (Need need in _needs) {
            float value = need.GetNormalizedBarValue();
            if (value < minValue && value <= normalizedBarActivationNeedValue) {
                minValue = value;
                interestingPlace = need.GetTypeOfNeed();
            }
        }  
        return interestingPlace;
    }

}

[System.Serializable]
public class Need
{
    [SerializeField] InterestingPlace typeOfNeed;
    float normalizedBar = 1;
    [SerializeField] float timeBeforeBeingEmptySecond;

    public void UpdateNeed() {
        normalizedBar -= Time.deltaTime / timeBeforeBeingEmptySecond;
        typeOfNeed.GetSlider().value = Mathf.Max(0,normalizedBar);
    }

    public void Fill()
    {
        normalizedBar = 1;
    }

    public InterestingPlace GetTypeOfNeed() {
        return typeOfNeed;
    }
    public float GetNormalizedBarValue() {
        return normalizedBar;
    }

}
