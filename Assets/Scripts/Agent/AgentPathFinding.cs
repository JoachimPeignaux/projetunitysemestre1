﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class AgentPathFinding : MonoBehaviour
{
    public Stack<(int, int)> _path = new Stack<(int, int)>(0);
    bool thinkingAboutAPath = false;

    [SerializeField] private float velocity = 20;
    [SerializeField] private float halfWanderMaxDistance = 0.4f;
    [SerializeField] private float detectionDistance = 0.1f;
    private AgentBrain agentBrain;
    private AgentMemory agentMemory;
    float maxCases;

    //(int, int) astarDestination = (-1, -1);

    private List<(int, int)> unvisitedCases = new List<(int, int)>();
    private List<(int, int)> unvisitedDeadEnd = new List<(int, int)>();

    void Start()
    {
        for (int i = 0; i < WorldGenerator.SIZE; i++)
        {
            for (int j = 0; j < WorldGenerator.SIZE; j++)
            {
                if (PathFounder.instance._grideMaze[j, i] == true) {
                    unvisitedCases.Add((j, i));
                    if (PathFounder.instance.IsADeadEnd((j,i)))
                    unvisitedDeadEnd.Add((j, i));
                }
            }
        }
        maxCases = unvisitedCases.Count;
        agentBrain = GetComponent<AgentBrain>();
        agentMemory = GetComponent<AgentMemory>();
        transform.position = new Vector3(1, 0, 1);
        Wander((1,1));
        GetComponentInChildren<TrailRenderer>().Clear();
    }

    // Update is called once per frame
    void Update() {
        (int, int) agentPosition = (Mathf.RoundToInt(transform.position.z), Mathf.RoundToInt(transform.position.x));
        if (unvisitedCases.Contains(agentPosition)){
            unvisitedCases.Remove(agentPosition);
            HUDManager.instance.SetPercentage(((maxCases-unvisitedCases.Count)/maxCases)*100f);
        }

        if (thinkingAboutAPath == false) {
            if (_path.Count != 0)
            {
                Vector3 direction = new Vector3(_path.Peek().Item2, 0, _path.Peek().Item1) - transform.position;
                if (direction.magnitude < detectionDistance)
                {
                    _path.Pop();
                    if (_path.Count == 0)
                    {
                        //prend la décision où aller
                        InterestingPlace interestingPlace = agentBrain.GetMostImportantNeed();
                        if (interestingPlace == null) {
                            Wander(agentPosition);
                        }
                        else
                        {
                            if (agentMemory.DoesKnowADestination(interestingPlace)){
                                Astar(agentMemory.GetCoordinate(interestingPlace, agentPosition));
                            } else {
                                Wander(agentPosition);
                            }

                        }

                    }
                    else
                    {
                        HUDManager.instance.ChangeState(HUDManager.GameState.running);
                    }
                }
                else
                    transform.position += direction.normalized * Time.deltaTime * velocity;
            }
        }

        //let the player decide the destination
        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit))
            {
                //astarDestination = (Mathf.RoundToInt(hit.point.z), Mathf.RoundToInt(hit.point.x));
            }
        }

    }

    public void SetDestination((int, int) position, (int, int) destination) {
        _path.Clear();
        _path = PathFounder.instance.Astar( position, destination);
        thinkingAboutAPath = false;
    }

    private void Astar((int, int) destination) {
        thinkingAboutAPath = true;
        (int, int) position = (Mathf.RoundToInt(transform.position.z), Mathf.RoundToInt(transform.position.x));
        Thread pathThread = new Thread(() => SetDestination(position, destination));
        HUDManager.instance.ChangeState(HUDManager.GameState.thinking);
        pathThread.Start();
    }

    private void Wander((int, int) position)
    {
        (int, int) destination;
        thinkingAboutAPath = true;
        if (unvisitedDeadEnd.Count != 0)
        {
            int index = Random.Range(0, unvisitedDeadEnd.Count);
            destination = unvisitedDeadEnd[index];
            unvisitedDeadEnd.RemoveAt(index);
        }
        else
        {
            Application.LoadLevel(Application.loadedLevel);
            return;
        }
        Thread pathThread = new Thread(() => SetDestination(position, destination));
        HUDManager.instance.ChangeState(HUDManager.GameState.thinking);
        pathThread.Start();
    }

}
