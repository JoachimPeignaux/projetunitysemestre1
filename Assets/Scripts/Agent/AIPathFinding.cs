﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class AIPathFinding : MonoBehaviour
{
    public Stack<(int, int)> _path = new Stack<(int, int)>(0);
    bool thinkingAboutAPath = false;

    [SerializeField] private float velocity = 20;
    [SerializeField] private float detectionDistance = 0.1f;
    private AgentMemory agentMemory;

    void Start()
    {
        agentMemory = GetComponent<AgentMemory>();
        int j = (int)Random.Range(0, (WorldGenerator.SIZE / 2) - 1)*2+1;
        int i = (int)Random.Range(0, (WorldGenerator.SIZE / 2) - 1)*2+1;
        transform.position = new Vector3(i, 0, j);
        _path.Push((j,i));
        GetComponentInChildren<TrailRenderer>().Clear();
    }

    // Update is called once per frame
    void Update() {
        (int, int) agentPosition = (Mathf.RoundToInt(transform.position.z), Mathf.RoundToInt(transform.position.x));
        if (thinkingAboutAPath == false) {
            if (_path.Count != 0)
            {
                Vector3 direction = new Vector3(_path.Peek().Item2, 0, _path.Peek().Item1) - transform.position;
                if (direction.magnitude < detectionDistance)
                {
                    _path.Pop();
                    if (_path.Count == 0)
                        Wander(agentPosition);
                }
                else
                    transform.position += direction.normalized * Time.deltaTime * velocity;
            }
        }
    }

    public void SetDestination((int, int) position, (int, int) destination) {
        _path.Clear();
        _path = PathFounder.instance.Astar( position, destination);
        thinkingAboutAPath = false;
    }

    private void Wander((int, int) position)
    {
        (int, int) destination;
        thinkingAboutAPath = true;
        int j = (int)Random.Range(0, (WorldGenerator.SIZE / 2) - 1)*2+1;
        int i = (int)Random.Range(0, (WorldGenerator.SIZE / 2) - 1)*2+1;
        Thread pathThread = new Thread(() => SetDestination(position, (j,i)));
        pathThread.Start();
    }

}
