﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AgentMemory : MonoBehaviour {

    private Dictionary<Type, List<(int, int)>> _memory = new Dictionary<Type, List<(int, int)>>();
    
    private List<AgentMemory> taughtMemories = new List<AgentMemory>(0);

    public void AddInformation(InterestingPlace kindOfPlace, (int, int) coord) {
        Type type = kindOfPlace.GetType();
        if (_memory.ContainsKey(type) == false) {
            _memory.Add(type, new List<(int, int)>());
        }
        if (_memory[type].Contains(coord)) {
            return;
        }
        taughtMemories.Clear();
        _memory[type].Add(coord);
        HUDManager.instance.SetHistory(gameObject.name + " has discovered a new " + type.Name);
    }

    public void AddInformationType(Type type, (int, int) coord) {
        if (_memory.ContainsKey(type) == false) {
            return;
        }
        if (_memory[type].Contains(coord)) {
            return;
        }
        taughtMemories.Clear();
        _memory[type].Add(coord);
        HUDManager.instance.SetHistory(gameObject.name + " has been taught a new " + type.Name);
    }

    public bool DoesKnowADestination(InterestingPlace kindOfPlace){
        Type kindOfPlaceType = kindOfPlace.GetType();
        return _memory.ContainsKey(kindOfPlaceType);
    }

    public (int, int) GetCoordinate(InterestingPlace kindOfPlace, (int, int) playerPosition) {
        Type kindOfPlaceType = kindOfPlace.GetType();
        
        if (_memory.ContainsKey(kindOfPlaceType)) {

            int minSqrDistance = int.MaxValue;
            (int, int) closestDestination = playerPosition;

            foreach ((int, int) place in _memory[kindOfPlaceType]) {
                int sqrDistance = (int)(Mathf.Pow(place.Item1 - playerPosition.Item1, 2) + Mathf.Pow(place.Item2 - playerPosition.Item2, 2));
                if (sqrDistance < minSqrDistance) {
                    minSqrDistance = sqrDistance;
                    closestDestination = place;
                }
            }
            return closestDestination;
        }

        throw new Exception("The memory doesn't have any destination like this in memory");
    }

    public void Teach(AgentMemory otherMemory)
    {
        if (taughtMemories.Contains(otherMemory) == false) {
            foreach (Type type in _memory.Keys)
            {
                foreach ((int, int) coord in _memory[type])
                {
                    otherMemory.AddInformationType(type, coord);
                }
            }
        }
        taughtMemories.Add(otherMemory);
    }

    protected virtual void OnTriggerEnter(Collider colilder)
    {
        if (colilder.CompareTag("Player") || colilder.CompareTag("AI")) {
            Teach(colilder.GetComponentInParent<AgentMemory>());
        }
    }

}
