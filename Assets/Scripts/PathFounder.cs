﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFounder {

	public bool[,] _grideMaze;
	private readonly int SIZE;
	public static PathFounder instance = null;

	public PathFounder(bool[,] gridMaze) {
		if (instance == null)
		{
			_grideMaze = gridMaze;
			SIZE = _grideMaze.GetLength(0);
			instance = this;
		}
	}

	public bool IsADeadEnd((int, int) coord) {
		return ((!_grideMaze[coord.Item1 + 1, coord.Item2] && !_grideMaze[coord.Item1 - 1, coord.Item2] && (!_grideMaze[coord.Item1, coord.Item2 - 1 ] || !_grideMaze[coord.Item1, coord.Item2 + 1]))
			 || (!_grideMaze[coord.Item1, coord.Item2 + 1] && !_grideMaze[coord.Item1, coord.Item2 - 1] && (!_grideMaze[coord.Item1 + 1, coord.Item2 ] || !_grideMaze[coord.Item1 - 1, coord.Item2])));
	}

    bool IsACorridor((int, int) coord) {
		return ((!_grideMaze[coord.Item1 + 1, coord.Item2] && !_grideMaze[coord.Item1 - 1, coord.Item2])
			 || (!_grideMaze[coord.Item1, coord.Item2 + 1] && !_grideMaze[coord.Item1, coord.Item2 - 1]));
	}

	List<(int, int)> GetSuccessors( (int, int) coord, (int, int) to)
	{
		List<(int, int)> listOfSuccessors = new List<(int, int)>(); 

		//right
		int i = 1;
		while (true){
			if ((coord.Item1 + i, coord.Item2) == to)
				return TheSuccessorIsDestination(to);

			if (coord.Item1 + i >= SIZE || _grideMaze[coord.Item1 + i, coord.Item2] == false) {
				listOfSuccessors.Add((coord.Item1 + ( i - 1 ), coord.Item2));
				break;
			}

			if (!IsACorridor((coord.Item1 + i, coord.Item2))) {
				listOfSuccessors.Add((coord.Item1 + i, coord.Item2));
				break;
			}

			i++;
		}


		//left
		i = 1;
		while (true)
		{
			if ((coord.Item1 - i, coord.Item2) == to)
				return TheSuccessorIsDestination(to);

			if (coord.Item1 - i < 0 || _grideMaze[coord.Item1 - i, coord.Item2] == false)
			{
				listOfSuccessors.Add((coord.Item1 - (i - 1), coord.Item2));
				break;
			}

			if (!IsACorridor((coord.Item1 - i, coord.Item2)))
			{
				listOfSuccessors.Add((coord.Item1 - i, coord.Item2));
				break;
			}

			i++;
		}

		//top
		i = 1;
		while (true)
		{
			if ((coord.Item1, coord.Item2 + i) == to)
				return TheSuccessorIsDestination(to);

			if (coord.Item2 + i >= SIZE || _grideMaze[coord.Item1, coord.Item2 + i] == false)
			{
				listOfSuccessors.Add((coord.Item1, coord.Item2 + (i - 1)));
				break;
			}

			if (!IsACorridor((coord.Item1, coord.Item2 + i)))
			{
				listOfSuccessors.Add((coord.Item1, coord.Item2 + i));
				break;
			}

			i++;
		}

		//bottom
		i = 1;
		while (true)
		{
			if ((coord.Item1, coord.Item2 - i) == to)
				return TheSuccessorIsDestination(to);

			if (coord.Item2 - i < 0 || _grideMaze[coord.Item1, coord.Item2 - i] == false)
			{
				listOfSuccessors.Add((coord.Item1, coord.Item2 - (i - 1)));
				break;
			}

			if (!IsACorridor((coord.Item1, coord.Item2 - i)))
			{
				listOfSuccessors.Add((coord.Item1, coord.Item2 - i));
				break;
			}

			i++;
		}
		return listOfSuccessors;
	}

	List<(int, int)> TheSuccessorIsDestination((int, int) to) {
		List<(int, int)> res = new List<(int, int)>(1);
		res.Clear();
		res.Add(to);
		return res;
	}

	public Stack<(int, int)> Astar((int, int) from, (int, int) to) {
		Stack<(int,int)> path = new Stack<(int,int)>();
		if ( to.Item1 < 0 || to.Item1 >= SIZE || to.Item2 < 0 || to.Item2 >= SIZE || from == to || _grideMaze[to.Item1, to.Item2] == false )
		//if destination is outdoor the grid or already reached
		{
			path.Push(from);
			return path;
		}

		int cptSecur = 0;
		Dictionary<(int, int), int> F = new Dictionary<(int, int), int>();
		Dictionary<(int, int), int> D = new Dictionary<(int, int), int>();
		Dictionary<(int, int), (int, int)> Prec = new Dictionary<(int, int), (int, int)>();
		Dictionary<(int, int), bool> marque = new Dictionary<(int, int), bool>();
		for (int j = 0; j < SIZE; j++)
		{
			for (int i = 0; i < SIZE; i++)
			{
				D[(j,i)] = int.MaxValue;
				F[(j, i)] = int.MaxValue;
				Prec[(j,i)] = (j,i);
				marque[(j,i)] = false;
			}
		}
		D[from] = 0;
		Prec[from] = from;
		F[from] = CalculeDistance(from, to); 

		int Vmin;
		do
		{
			//recherche du sommet x non ficé de plus petite étiquette
			(int, int) x = (-1,-1);
			Vmin = int.MaxValue;
			for (int j = 0; j < SIZE; j++)
			{
				for (int i = 0; i < SIZE; i++)
				{
					if (marque[(j,i)] == false && F[(j,i)] < Vmin)
					{
						x = (j,i);
						Vmin = F[(j,i)];
					}
				}
			}

			//Mise à jour des successeurs non fixés de x
			if (Vmin < int.MaxValue)
			{
				marque[x] = true;
				List<(int, int)> successors = GetSuccessors(x, to);
				for (int ind = 0; ind < successors.Count; ind++)
				{
					if (marque[successors[ind]] == false && D[x] + 1 < D[successors[ind]])
					{
						D[successors[ind]] = D[x] + 1;
						F[successors[ind]] = D[x] + 1 + CalculeDistance(from, to);
						Prec[successors[ind]] = x;
					}
				}
			}
			cptSecur++;
		} while (Vmin != int.MaxValue && Prec[to] == to && cptSecur <= SIZE * SIZE + 1);

		if (Prec[to] != to) //il y a un chemin
		{
			(int, int) courant = to;
			cptSecur = 0;
			do
			{
				path.Push(courant);
				courant = Prec[courant];
				cptSecur++;
			} while (courant != from && cptSecur <= SIZE * SIZE + 1);
		}
		path.Push(from);
		return path;
	}

	int CalculeDistance((int, int) a, (int, int) b) {
		return (int)(Mathf.Pow((a.Item1 - b.Item1), 2) + Mathf.Pow((a.Item2 - b.Item2), 2));
	}

}
